import React,{useState} from 'react';

import './App.css';
import Header from './components/header';
import Figure from './components/figure';
import WrongLetter from './components/WrongLetter';
import Word from './components/word';
const words = ['application', 'programming', 'interface', 'wizard'];

let selectedWord = words[Math.floor(Math.random() * words.length)];

let playable = true;

const correctLetters = [];
const wrongLetters = [];
function App() {
  return (
<>
    <Header/>
    <div className="game-container">
      <Figure/>
      <WrongLetter />
      <Word/>
    </div>
</>
  );
}

export default App;
