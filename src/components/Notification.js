import React from 'react'

function Notification() {
    return (
        <div className="notification-container" id="notification-container">
            <p>You have already entered this letter</p>
        </div>
    )
}

export default Notification
